# React & Redux Tutorial Notes

Notizen zu einem react.js und redux.js Tutorial.

[TUTORIAL__->>](Tutorial.md)

## JavaScript 

```javascript
export function fetchPosts() {
  console.log('fetching');
  return function(dispatch) {
    var a = fetch('https://jsonplaceholder.typicode.com/posts')
    .then(res => res.json())
    .then(posts => dispatch({
      type: FETCH_POSTS,
      paylode: posts
    }));
    console.log(a)
  }
}
```


```javascript

export const fetchPosts = () => dispatch => {
  fetch('https://jsonplaceholder.typicode.com/posts')
  .then(res => res.json())
  .then(posts => 
    dispatch({
      type: FETCH_POSTS,
      payload: posts
    })
  );
};

```

## ES6 style 


Link zu dem [YouTubeViedo](https://www.youtube.com/watch?v=93p3LxR9xfM)