# Tutorial - react redux

## Table of Content

1. Prviding the Store
2. Connection the Components


## Weitere Links: 
1. https://soongding87.github.io/2018-12-27-Redux/



## Providing the Store
provide store with `<Provider store={store} />` react Component.  

![store](docs/01_props_store.PNG)


## Connecting the Components 
Um ein `Component` an einen Redux Store zu binden, gitb es die function `connect`.  
Die `connect` funktion nimmt 2 optionale argumente an. 

* **Argument1** `mapStateToProps` :  
  Wird nach jeder Änderung im state store aufgerufen.

* **Argument2** `mapDispatchToProps`:  
  Zwei möglichkeiten, als Funktion oder als Object.  
    
  **Funktion**:  
    
  **Object**:  
  Es wird empfolen die `object shorthand` zu verwenden. Hierbei wird ein Object mit `action creators` übergeben. Diese werden dann automatisch  ❗❗ dispatched. 😂😂  

Der Normale auf ruf der funktion sieht so aus: 

```javascript

const mapStateToProps = (state, ownProps) => ({
  // ... computed data from state and optionally ownProps
})

const mapDispatchToProps = {
  // ... normally is an object full of action creators
}

// `connect` returns a new function that accepts the component to wrap:
const connectToStore = connect(
  mapStateToProps,
  mapDispatchToProps
)
// and that function returns the connected, wrapper component:
const ConnectedComponent = connectToStore(Component)

// We normally do both in one step, like this:
connect(
  mapStateToProps,
  mapDispatchToProps
)(Component)

```

Die Componente wird dann mit `export default connect(null, {var})(Component)` exportiert.

> Info: So cann man sich die connect Funktin vilt besser vorstellen.  
> `connect(` **Daten die angezeit weden sollen.** `,` **Die Action Funktion** `)`.

Danach sollte die Funktion dann in den `props` sichtbar sein. 

![function in props](docs/02_addTodoFunction_asProp.PNG)


_sebastian schilling_